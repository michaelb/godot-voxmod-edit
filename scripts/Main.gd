
const WorldViewer = preload('../scenes/WorldViewer.tscn')

onready var gui = get_node('GUI/GUIOverlay')
onready var viewer = get_node('Viewer')

var current_viewer = null

func _ready():
    gui.connect('mod_ready', self, 'show_mod_info')
    gui.connect('world_ready', self, 'show_world')
    gui.connect('voldata_ready', self, 'show_voldata')
    gui.connect('pattern_ready', self, 'show_pattern')
    gui.connect('save_requested', self, 'do_save')

func show_mod_info():
    pass

func show_voldata(info):
    var modloader = gui.modloader
    var world_viewer = WorldViewer.instance()
    var voldata = info['__obj']
    world_viewer.setup_voldata(modloader, voldata)
    viewer.add_child(world_viewer)
    gui.disable_saving()

func show_world(info):
    var modloader = gui.modloader
    if current_viewer != null:
        current_viewer.queue_free()
    current_viewer = WorldViewer.instance()
    current_viewer.setup(modloader)
    viewer.add_child(current_viewer)
    gui.disable_saving()

func show_pattern(info):
    var modloader = gui.modloader
    if current_viewer != null:
        current_viewer.queue_free()
    current_viewer = WorldViewer.instance()
    var pattern = info['__obj']
    current_viewer.setup_pattern(modloader, pattern)
    viewer.add_child(current_viewer)
    gui.enable_saving()

func do_save():
    current_viewer.do_save()

