const VoxelTerrain = preload('res://submodules/godot-voxterrain/VoxelTerrain.tscn');
const EditCursor = preload('res://submodules/godot-voxterrain/EditCursor.tscn');
const PannableCamera = preload('res://submodules/godot-pannable-camera/PannableCamera.tscn');
const ModLoader = preload('res://submodules/godot-voxmod/ModLoader.gd')

var voxel_terrain = null
var entity_manager = null
var camera = null

var modloader = null
var voldata = null
var pattern = null

onready var save_button = get_node('ViewerGUI/SaveButton')

func setup(modloader):
    self.modloader = modloader
    #modloader = ModLoader.new()
    #modloader.load_dir('res://submodules/godot-voxmod/tests/data/testmod1')

func setup_pattern(modloader, pattern):
    self.modloader = modloader
    self.pattern = pattern

func setup_voldata(modloader, voldata):
    self.modloader = modloader
    self.voldata = voldata

func _ready():
    camera = PannableCamera.instance()
    add_child(camera)

    # Set up voxel terrain and entity manager
    voxel_terrain = VoxelTerrain.instance()
    voxel_terrain.set_mod(modloader)
    add_child(voxel_terrain)
    voxel_terrain.set_name('VoxelTerrain')

    # Generates new world
    var world_data = modloader.generate_world()

    save_button.hide()
    if pattern != null:
        # If we are loading a pattern to edit
        voxel_terrain.set_pattern(pattern)
        var edit_cursor = EditCursor.instance()
        edit_cursor.set_camera(camera)
        edit_cursor.add_to_terrain(voxel_terrain)
        add_child(edit_cursor)
        voxel_terrain.render_all()
        save_button.show()
    elif voldata == null:
        voxel_terrain.set_worldata(world_data)
        # Finally update the results
        camera.connect('moved', self, 'adjust_simulation_center')
    else:
        # If its voldata, load it and create an edit cursor
        voxel_terrain.load_voldata(voldata)
        var edit_cursor = EditCursor.instance()
        edit_cursor.set_camera(camera)
        edit_cursor.add_to_terrain(voxel_terrain)
        add_child(edit_cursor)
        voxel_terrain.render_all()

func adjust_simulation_center():
    # Refresh based on camera center, minimum (1 radius, thus 1 tch)
    voxel_terrain.update_mesh(camera.target, 32)

func do_save():
    modloader.save_edited_static_pattern(pattern)

