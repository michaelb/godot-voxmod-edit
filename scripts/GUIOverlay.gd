const ModLoader = preload("res://submodules/godot-voxmod/ModLoader.gd")

onready var open_mod_dialog = get_node('OpenModDialog')
#onready var tree = get_node('HSplitContainer/VBoxContainer/Tree')
onready var barray = get_node('HSplitContainer/VBoxContainer/VButtonArray')
onready var opened_mod_label = get_node('HBoxContainer/OpenedModLabel')
onready var save_button = get_node('HSplitContainer/VBoxContainer2/Container/SaveButton')

var modloader = null
var mod_dir = null
var file_loaded = null
var info_loaded = null

signal mod_ready
signal file_ready
signal world_ready
signal voldata_ready
signal pattern_ready

signal save_requested

func _ready():

    #######################################
    # FOR TESTING:
    mod_dir = '/home/michaelb/private/programming/godot-voxmod/tests/data/testmod1'
    #######################################

    if mod_dir != null:
        load_mod_at(mod_dir)

    disable_saving()

var file_buttons = {}
var obj_buttons = {}

func do_button_refresh():
    barray.clear()
    var i = 0
    if file_loaded != null:
        # When we load a file, we then init the barray to be object
        # list in that file
        opened_mod_label.set_text(mod_dir)
        barray.add_button('<----')
        i += 1

        print(modloader.objs_by_filepath.keys())
        var objs = []
        if modloader.objs_by_filepath.has(file_loaded):
            objs = modloader.objs_by_filepath[file_loaded]
        for info in objs:
            var type = info['__type']
            var name = info['name']
            var label = type + ': ' + name
            barray.add_button(label)
            obj_buttons[i] = info
            i += 1

        emit_signal('file_ready')
    elif modloader != null:
        # When we only have a mod loaded, then we init the barray to be
        # all files in the mod
        opened_mod_label.set_text(mod_dir)
        for full_path in modloader.file_list:
            var relpath = full_path.substr(mod_dir.length() + 1, full_path.length())
            var fn = full_path.get_file()
            barray.add_button(relpath)
            file_buttons[i] = full_path
            i += 1
            #barray.set_button_tooltip(i, relpath)
        emit_signal('mod_ready')


func load_something_at(path):
    # TODO have this figure out if its a cfg or not and then walk up to the mod
    # directory, and load the mod, then drill down and load the file that was
    # actually selected. Eventually allow CLI usage of this so that
    pass

func load_mod_at(mod_dir):
    self.mod_dir = mod_dir
    modloader = ModLoader.new()
    modloader.load_dir(mod_dir)
    do_button_refresh()

func load_info_obj(info):
    print("Loading:")
    print(info)
    var signals = {
        'world': 'world_ready',
        'voldata': 'voldata_ready',
        'pattern': 'pattern_ready',
    }

    var type = info['__type']
    if signals.has(type):
        emit_signal(signals[type], info)
    else:
        print("WARNING: No special behavior for type " + type)

func _on_Button_pressed():
    open_mod_dialog.popup()

func _on_OpenModDialog_dir_selected(dir):
    # Load mod!
    load_mod_at(dir)

func _on_VButtonArray_button_selected(button_idx):
    # Load Given button
    if file_loaded != null:
        if button_idx == 0:
            # Clicking '<----', should undo
            file_loaded = null
            do_button_refresh()
        else:
            # Otherwise we load a particular obj
            info_loaded = obj_buttons[button_idx]
            load_info_obj(info_loaded)
    else:
        file_loaded = file_buttons[button_idx]
        do_button_refresh()

func enable_saving():
    save_button.show()

func disable_saving():
    save_button.hide()

func _on_SaveButton_pressed():
   emit_signal('save_requested')
