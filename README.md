# godot-voxmod-edit

Editor for mod system for a particular flavor of voxel-type games.

This is generally just made for my own games, but I'm releasing it
freely so it can be used for other folks if their uses happen to align
with mine! That said, keeping documentation at top quality is not a
high priority for me.

## requirements

This depends on several other of my godot packages:

* `godot-voxtools`
* `godot-voxmod`
* `godot-voxterrain`
* `godot-pannable-camera`

Install those in a subdirectory called `submodules' so they can be
found.


